const express = require('express')
const mysql = require('mysql')
const myconn = require('express-myconnection')


const app = express()
app.set('port', process.env.PORT || 9000)

const bdOptions = {
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '12345',
    database: 'newstyle'
}

// middlewares ---------------------------------------------------------

app.use(myconn(mysql, dbOptions, 'single'))



// routes --------------------------------------------------------------
app.get('/', (req, res)=>{
    res.send('welcome to my API')
})
// server running ------------------------------------------------------
app.listen(app.get('port'), ()=>{
    console.log('server running on',app.get('port'))
})